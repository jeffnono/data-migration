package com.spring.batch.job.listener;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;
import org.springframework.stereotype.Service;

import com.modules.mapper.JaxbMapper;

import datamigartion.BeanTable;
import datamigartion.Column;
import datamigartion.Table;

@Service
public class ReportJobListener {

	@BeforeJob
	public void beforeJob(JobExecution jobExecution) {
		String path = jobExecution.getJobParameters().getString("path");
		BeanTable bbt = JaxbMapper.fromXmlPath(path, BeanTable.class);
		//BeanTable bt = getFileInfo(path);
		// 封装所有的source元素
		StringBuffer stringSource = new StringBuffer();
		// 封装所有的target元素
		StringBuffer stringTarget = new StringBuffer();
		// 获取所有的column信息
		List<Column> coluList = bbt.getTable().getColuList();

		if (null != coluList && coluList.size() > 0) {
			for (int i = 0; i < coluList.size(); i++) {
				if (null != coluList.get(i)
						&& null != coluList.get(i).getSource()
						&& !coluList.get(i).getSource().isEmpty()
						&& null != coluList.get(i).getTarget()
						&& !coluList.get(i).getTarget().isEmpty()) {

					stringSource.append(coluList.get(i).getSource())
							.append(",");
					stringTarget.append(coluList.get(i).getTarget())
							.append(",");
				}
			}
		}
		String strSource = stringSource.toString().substring(0,
				stringSource.toString().length() - 1);
		// 获得所有非空数据
		String[] strInfo = strSource.split(",");
		// 获取主键标识
		String idName = strInfo[0];
		// 封装添加参数
		StringBuffer mhBuffer = new StringBuffer();
		for (int k = 0; k < strInfo.length; k++) {
			mhBuffer.append(":").append(strInfo[k]).append(",");
		}
		String mBuffer = mhBuffer.toString()
				.substring(0, mhBuffer.length() - 1);
		String strTarget = stringTarget.toString().substring(0,
				stringTarget.toString().length() - 1);
		jobExecution.getExecutionContext().put("selectClause", strSource);
		jobExecution.getExecutionContext().put("fromClause",
				bbt.getTable().getTabSource());
		jobExecution.getExecutionContext().put("minValue", "1");
		jobExecution.getExecutionContext().put("maxValue", "10");
		jobExecution.getExecutionContext().put("sortKey1", idName);
		jobExecution.getExecutionContext().put(
				"targetSQL",
				"insert into " + bbt.getTable().getTabTarget() + "(" + strTarget
						+ ") values(" + mBuffer + ")");
		jobExecution.getExecutionContext().put("cloumnsMapping", coluList);
	}

	@AfterJob
	public void afterJob(JobExecution jobExecution) {
		System.out.println("selectClause"
				+ jobExecution.getExecutionContext().get("selectClause"));
	}

	public static BeanTable getFileInfo(String xmlStr) {
		File file = new File(xmlStr);
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext
					.newInstance(new Class[] { BeanTable.class, Table.class,
							HashMap.class, Column.class });
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			BeanTable beanTable = (BeanTable) jaxbUnmarshaller.unmarshal(file);
			return beanTable;
		} catch (JAXBException e) {
			e.getMessage();
			e.printStackTrace();
		}
		return null;
	}
}
