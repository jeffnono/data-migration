package com.spring.batch.fieldmapper;

import org.springframework.stereotype.Service;

@Service
public class DefaultFieldMapper implements FieldMapper{
	
	@Override
	public Object mapping(Object item) {
		return item;
	}

}
