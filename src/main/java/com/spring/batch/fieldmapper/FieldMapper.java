package com.spring.batch.fieldmapper;

public interface FieldMapper<I, O> {
  public I mapping(O item);
}
