package com.spring.batch.fieldmapper;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class JdbcFieldMapper implements FieldMapper<String,String> {

	@Autowired
	@Qualifier(value="dataSourceReader")
	DataSource dataSource;
	
	@Override
	public String mapping(String item) {
		return "jdbc";
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

}
