package com.spring.batch.step.listener;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeStep;

public class StepListener {
	protected int countBeforeStep = 0;
	protected int countAfterStep = 0;

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		// stepExecution.getExecutionContext().put(key, value);
	}

	@AfterJob
	public ExitStatus afterStep(StepExecution stepExecution) {
		countAfterStep++;
		return null;
	}
}
