package com.spring.batch.rowmapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

@SuppressWarnings("rawtypes")
public class MapRowMapper implements RowMapper<Map> {

	public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
		Map<String, Object> map = new HashMap<String, Object>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		// The column count starts from 1
		for (int i = 1; i < columnCount + 1; i++) {
			String name = rsmd.getColumnName(i);
			map.put(name, rs.getString(name));
		}
		return map;
	}

}
