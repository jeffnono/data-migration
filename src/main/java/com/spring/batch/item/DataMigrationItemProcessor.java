package com.spring.batch.item;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.spring.batch.fieldmapper.CompositeFieldMapper;

import datamigartion.Column;

@Service
@Scope(value = "step")
public class DataMigrationItemProcessor implements ItemProcessor<Map<String, Object>, Map<String, Object>>{
  protected static final Logger logger = Logger.getLogger(DataMigrationItemProcessor.class);
  @Value("#{jobExecutionContext[cloumnsMapping]}")
	private List<Column> cloumnsMapping;
  
	@Autowired
	private CompositeFieldMapper compositeFieldMapper;
	
	public Map<String, Object> process(Map<String, Object> rows) throws Exception {
		compositeFieldMapper.setCloumnsMapping(cloumnsMapping);
		compositeFieldMapper.mapping(rows);
		return rows;
	}

	public void setCompositeFieldMapper(CompositeFieldMapper compositeFieldMapper) {
		this.compositeFieldMapper = compositeFieldMapper;
	}

}
