package datamigartion;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "table")
@XmlAccessorType(XmlAccessType.FIELD)
public class Table {
	@XmlAttribute(name = "source")
	private String tabSource;
	@XmlAttribute(name = "target")
	private String tabTarget;
	@XmlAttribute(name = "status")
	private String tabStatus;

	@XmlElement(name = "column")
	private List<Column> coluList = new ArrayList<Column>();

	public String getTabSource() {
		return tabSource;
	}

	public void setTabSource(String tabSource) {
		this.tabSource = tabSource;
	}

	public String getTabTarget() {
		return tabTarget;
	}

	public void setTabTarget(String tabTarget) {
		this.tabTarget = tabTarget;
	}

	public String getTabStatus() {
		return tabStatus;
	}

	public void setTabStatus(String tabStatus) {
		this.tabStatus = tabStatus;
	}

	public List<Column> getColuList() {
		return coluList;
	}

	public void setColuList(List<Column> coluList) {
		this.coluList = coluList;
	}

}
