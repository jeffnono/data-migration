package datamigartion;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "column")
@XmlAccessorType(XmlAccessType.FIELD)
public class Column {

	@XmlAttribute(name = "source")
	private String source;
	@XmlAttribute(name = "target")
	private String target;
	@XmlAttribute(name = "fieldMapper")
	private String fieldMapper;
	
	@XmlElement(name = "map", required = true)
	private List<Map> keyListMap;

	public String getSource() {
		return source.toUpperCase();
	}

	public void setSource(String source) {
		this.source = source.toUpperCase();
	}

	public String getTarget() {
		return target.toUpperCase();
	}

	public void setTarget(String target) {
		this.target = target.toUpperCase();
	}

	public List<Map> getKeyListMap() {
		return keyListMap;
	}

	public void setKeyListMap(List<Map> keyListMap) {
		this.keyListMap = keyListMap;
	}

	public String getFieldMapper() {
		return fieldMapper;
	}

	public void setFieldMapper(String fieldMapper) {
		this.fieldMapper = fieldMapper;
	}
	
}
