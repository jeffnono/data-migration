
import java.io.File;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class App {

	public static void main(String[] args) {
		String[] springConfig = { "spring/batch/config/database.xml", "spring/batch/config/context.xml",
				"spring/batch/jobs/job-report.xml" };

		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job job = (Job) context.getBean("reportJob");
		try {
			ClassPathResource resources = new ClassPathResource("spring/batch/db-mapping");
			File[] fs = resources.getFile().listFiles();
			if (null != fs && fs.length > 0) {
				for (int i = 0; i < fs.length; i++) {
					String path1 = fs[i].toString();
					JobParametersBuilder builder = new JobParametersBuilder();
					builder.addString("path", path1);
					JobParameters jobParameters = builder.toJobParameters();
					jobLauncher.run(job, jobParameters);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
